function get_uploads()
{
    var elements = $();
    $.ajax({
        url: 'router.php',
        data: {method: 'get_uploads'},
        type: 'get',
        dataType: 'JSON',
        success: function (resp) {
            $.each(resp, function (index, img) {
                elements = elements.add('<img class="img_ok" src="'+img+'">')
                //$('#gallery').fadeOut(500, function () {});
                $('#gallery').html('');
                $('#gallery').append(elements);
                //$('#gallery').fadeIn(10, function () {});
            })
        }
    })
}

function clear_gallery() {
    $('#clear_gallery').click(function () {
        $.ajax({
            url: 'router.php',
            data: {method: 'clear_gallery'},
            type: 'get',
            success: function (resp) {
                if(resp === 'ok') {
                    $('.img_ok').remove();
                }
            }
        })
    })
}

function client() {
    $('#client').click(function () {
        $.ajax({
            url: 'router.php',
            data: {
                method: 'client',
                url: $('#url').val()
            },
            type: 'get',
            success: function (resp) {
                if(resp === 'ok') {
                    alert('OK, please wait small time');
                }
            }
        })
    })
}

function worker_start() {
    $('#worker_start').click(function () {
        $.ajax({
            url: 'router.php',
            data: {
                method: 'worker_start',
            },
            type: 'get',
            success: function (resp) {
                if(resp === 'ok') alert('Worker успешно добавлен');
            }
        })
    })
}

function worker_stop() {
    $('#worker_stop').click(function () {
        $.ajax({
            url: 'router.php',
            data: {
                method: 'worker_stop',
            },
            type: 'get',
            success: function (resp) {
                if(resp === 'ok') alert('Все worker-ы успешно остановлены');
            }
        })
    })
}


$(document).ready(function () {
    setInterval(function () {
        get_uploads();
    }, 10000);
    clear_gallery();
    client();
    worker_start();
    worker_stop();
})