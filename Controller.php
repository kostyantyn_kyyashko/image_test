<?php
require_once 'includes.php';
class Controller
{

    public static function get_uploads()
    {
        $path_to_uploads_dir = dirname(__FILE__) . '/uploads';
        $files = scandir($path_to_uploads_dir);
        $out = [];
        foreach ($files as $img) {
            $full_path_img = $path_to_uploads_dir . '/' . $img;
            if (is_file($full_path_img) && in_array(mime_content_type($full_path_img), ['image/jpeg', 'image/png', 'image/gif'])) {
                $out[] = UPLOADS_RELATIVE_URL . '/' . $img;
            }
        }
        shuffle($out);
        echo json_encode($out);
    }

    public static function client()
    {
        //$url = 'https://ru.123rf.com/%D0%A4%D0%BE%D1%82%D0%BE-%D1%81%D0%BE-%D1%81%D1%82%D0%BE%D0%BA%D0%B0/%D0%9B%D1%8E%D0%B1%D0%BE%D0%B2%D1%8C.html?sti=n7kvxcvjq7bav28kre|';
        $url = $_GET['url'];
        $handler = new ImgHandler();
        $host_http_protocol = $handler->get_http_protocol($url);
        $host = $handler->get_host($url);

        $html = $handler->webPageGet($url);
        $src_all = $handler->get_all_src($html);

        $gclient = new GearmanClient();
        $gclient->addServer();

        foreach ($src_all as $src)
        {
            $img_file_name = $handler->get_img_file_name_from_src($src);
            $img_file_name = explode('?', $img_file_name)[0];
            $full_upload_path = UPLOAD_PATH . "/$img_file_name";

            $src = $handler->src_to_canonical_url($src, $host_http_protocol, $host);

            $data_for_gearman = [
                'src' => $src,
                'full_upload_path' => $full_upload_path
            ];
            $gclient->doBackground('get_and_save_image', serialize($data_for_gearman));
        }
        echo 'ok';
    }

    public static function clear_gallery()
    {
        $files = scandir(UPLOAD_PATH);
        foreach ($files as $img) {
            $img = UPLOAD_PATH . "/$img";
            unlink($img);
        }
        echo 'ok';
    }

    public static function worker_start() {
        $command = "php " . APP_PATH . "/worker.php > /dev/null &";
        shell_exec($command);
        echo 'ok';
    }

    public static function worker_stop() {
        $command = "ps ax | grep worker.php | awk '{print $1}' | xargs kill";
        shell_exec($command);
        echo 'ok';
    }
}
