<?php
$worker = new GearmanWorker();
$worker->addServer();
$worker->addFunction('get_and_save_image', 'get_and_save_image');
function get_and_save_image(GearmanJob $job){
    $data = $job->workload();
    $data = unserialize($data);
    $src = $data['src'];
    $full_upload_path = $data['full_upload_path'];
    $img_data = getimagesize($src);
    $width = intval($img_data[0]);
    $height = intval($img_data[1]);
    $mime = $img_data['mime'];
    if ($width >= 200 && $height >= 200 && in_array($mime, ['image/jpeg', 'image/png', 'image/gif'])) {
        $img_binary = file_get_contents($src);
        $im = new Imagick();
        $draw = new ImagickDraw();
        $pixel = new ImagickPixel( 'red' );
        try {
            $im->readImageBlob($img_binary);
            $im->thumbnailImage(0, 200);
            $im->cropImage(200, 200, 0, 0);
            $im->setImageFormat(str_replace('image/', '', $mime));
            $draw->setFillColor($pixel);
            $draw->setFont('Bookman-DemiItalic');
            $draw->setFontSize(20);
            $im->annotateImage($draw, 10, 190, -45, 'Beautiful Day :)');
            file_put_contents($full_upload_path, $im);
        }
        catch (Exception $e) {
            echo $e->getMessage() . "\n";
        }
    }
    return;
}
while ($worker->work()){}

