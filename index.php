<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>test</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
          crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
          crossorigin="anonymous">
    <link rel="stylesheet" href="css/test.css">
    <script
            src="https://code.jquery.com/jquery-3.5.1.js"
            integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <script src="js/test.js"></script>
</head>
<body>
<div class="container">
    <div>
        <h4>
            <a href="https://bitbucket.org/kostyantyn_kyyashko/image_test/src/master/">Исходный код</a>
        </h4>
        <h4>Сори, я немного добавил задаче: с интервалом в неск секунд все скачанные картинки выводятся
            в случайном порядке. Уж очень красочные картинки ))</h4>
        <h4>Скачивание изображений происходит с использованием сервера очередей Gearman, все тяжелые по времени операции
        вынесены в очередь, что 1) не тормозит страницу и задача не вылетит по браузерному таймауту; 2) можно распараллеливать
        процесс загрузки картинок в несколько потоков. Самое просто управление очередью вынесено в интерфейс. Вообще много нюансов.
        После первой загрузки страницы панно с картинками появится через неск. секунд, далее процесс непрерывный</h4>
        <h4>URL:</h4>
        <h4>рабочий пример URL, много картинок</h4>
        <h5>https://ru.123rf.com/%D0%A4%D0%BE%D1%82%D0%BE-%D1%81%D0%BE-%D1%81%D1%82%D0%BE%D0%BA%D0%B0/%D0%9B%D1%8E%D0%B1%D0%BE%D0%B2%D1%8C.html?sti=n7kvxcvjq7bav28kre|</h5>
        <input type="text" id="url" class="form-control" placeholder="Введите полный URL страницы с картинками">
    </div>
    <table class="table" id="control">
        <tbody>
        <tr>
            <td><button class="btn btn-info" id="client">Получить изображения</button> </td>
            <td><button class="btn btn-warning" id="clear_gallery">Очистить галерею</button> </td>
            <td><button class="btn btn-primary" id="worker_start">Запустить worker</button> </td>
            <td><button class="btn btn-default" id="worker_stop">Остановить worker-ы</button> </td>
        </tr>
        </tbody>
    </table>
    <div id="gallery"></div>

</div>
</body>
</html>