<?php
require_once 'ImgHandler.php';
require_once 'Controller.php';

define('APP_PATH', dirname(__FILE__));
define('UPLOAD_PATH', APP_PATH . "/uploads");
define('UPLOADS_RELATIVE_URL', '/test/uploads');