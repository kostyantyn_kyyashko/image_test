<?php
class ImgHandler
{
    public function webPageGet($url, $cookies_in = '', $is_post = false, $post_fields = null, $noProxy = false)
    {
        $options = [
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => true,     //return headers in addition to content
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
            CURLINFO_HEADER_OUT    => true,
            CURLOPT_SSL_VERIFYPEER => false,     // Disabled SSL Cert checks
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,

        ];

        $ch = curl_init( $url );
        curl_setopt_array( $ch, $options );
        $rough_content = curl_exec( $ch );
        $err     = curl_errno( $ch );
        $errmsg  = curl_error( $ch );
        $header  = curl_getinfo( $ch );
        curl_close( $ch );

        $header_content = substr($rough_content, 0, $header['header_size']);
        $body_content = trim(str_replace($header_content, '', $rough_content));

        return $body_content;
    }

    public function get_img_file_name_from_src($src)
    {
        $src = explode('/', $src);
        return end($src);
    }

    public function get_http_protocol($url)
    {
        $protocol = parse_url($url, PHP_URL_SCHEME);
        if ($protocol == 'http' || $protocol == 'https'){
            return $protocol;
        }
        return false;
    }

    public function get_host($url)
    {
        return parse_url($url, PHP_URL_HOST);
    }

    public function src_to_canonical_url($src, $host_http_protocol, $host) {
        if($this->get_host($src) && $this->get_http_protocol($src)) {
            return $src;
        }
        elseif(substr($src, 0, 2) == '//') {
            return $host_http_protocol . ':' . $src;
        }
        elseif (substr($src, 0, 1) == '/') {
            return $host_http_protocol . '://' . $host . $src;
        }
        return $host_http_protocol . '://' . $host . '/' . $src;
    }

    public function get_all_src($html)
    {
        $doc = new DOMDocument();
        @$doc->loadHTML($html);
        $tags = $doc->getElementsByTagName('img');
        $src_all = [];
        foreach ($tags as $tag) {
            $src_all[] = $tag->getAttribute('src');
        }
        return $src_all;
    }


}
