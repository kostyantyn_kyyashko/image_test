<?php
require_once 'Controller.php';
$method = $_GET['method'];
if(method_exists('Controller', $method)) {
    Controller::$method();
}
